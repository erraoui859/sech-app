import 'package:flutter/material.dart';
import 'package:sech_app/acceuil.dart';
import 'package:sech_app/main.dart';
void main()=> runApp(MyApp());
class MyApp  extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title:'flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
class Settings  extends StatefulWidget {


  @override
  State<Settings > createState() => _SettingsState();
}

class _SettingsState extends State<Settings > {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Colors.white,
      appBar: AppBar(
        backgroundColor:Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.logout,color: Colors.black),
          onPressed:(){
            Navigator.pop(context,
             MaterialPageRoute(builder: (context)=>MyHomePage()));
          }
        ),
      ),
      body: Center(
        child: Text('Settings',style: TextStyle(color: Colors.red)),
      ),
      
    );
  }
}