import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:sech_app/acceuil.dart';
import 'package:sech_app/bouttons.dart';
import 'package:sech_app/login.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
 @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool mdp=true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        child: Stack(
          children: [
            Positioned.fill(
              child: Opacity(
                opacity: 0.4,
                child: Image.asset('images/seches.jpg_.webp',
                fit: BoxFit.cover,
                ),
              ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment:CrossAxisAlignment.center,
                  children: [
                    ClipOval(
                      child: Container(
                        height:250,
                        width: 250,
                        color:Colors.grey.shade400,
                        alignment: Alignment.center,
                        child: Image.asset('images/logo_gep.png',fit: BoxFit.cover,color: Colors.green.shade500,),
                      ),
                    ),
                    SizedBox(height: 30,),
                    Text('Bienvenue',
                    style: TextStyle(fontSize: 50,
                    fontStyle: FontStyle.normal,
                    color: Colors.white,
                    fontWeight: FontWeight.bold
                    ),
                    ),
                    SizedBox(height: 10,),
                    const Text('Séchage des produits agro-alimentaires',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                    ),
                    SizedBox(height: 60,),
                    ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                         MaterialPageRoute(builder: (context)=>LoginPage()));
                      },
                      child: Text('Connexion',style: TextStyle(fontSize: 30),),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.green),
                        padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical:20,horizontal:100)),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  side:BorderSide(color: Colors.grey),
                  borderRadius:BorderRadius.circular(35) ,
                  )),
                      ),
                       )
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}