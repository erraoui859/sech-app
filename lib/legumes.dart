import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:sech_app/acceuil.dart';
import 'package:sech_app/debut_sech.dart';


void main()=>runApp(MyApp());

class Legumes extends StatefulWidget {

  @override
  State<Legumes> createState() => _LegumesState();
}

class _LegumesState extends State<Legumes> {
  
   
   int _current=0;
   dynamic _selectedLegume = {};

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation:0,
        backgroundColor: Colors.transparent,
        title: Text('Selctionner votre legume',
        style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 25),
        ),
        leading: IconButton(
          icon:Icon(Icons.arrow_back,color: Colors.black,),
          onPressed: (){
            Navigator.pop(context,
            MaterialPageRoute(builder: (context)=> Home()));
          }, 
          ),
        centerTitle:true,
      ),
     
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: CarouselSlider(
          options: CarouselOptions(
            height: 450,
            aspectRatio: 16/9,
            viewportFraction: 0.70,
            enlargeCenterPage: true, 
          ),
          items: datalist.map(( DataLegumes){
            return Builder(
              builder: (context) {
                return Column(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            if (_selectedLegume==DataLegumes)
                            _selectedLegume={};
                            else 
                            _selectedLegume=DataLegumes;
                          });
                        },
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 300),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            border: _selectedLegume==DataLegumes ? Border.all(
                              color: Colors.blue.shade500,
                              width:  3
                            ):null ,
                            boxShadow: _selectedLegume ==DataLegumes ? [
                              BoxShadow(
                                color:Colors.blue.shade100,
                                blurRadius: 30,
                                offset: Offset(0, 10),
                              )
                            ] :  [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 20,
                                offset: Offset(0, 5)
                              )
                            ],
                          ),
                          child: SingleChildScrollView(
                            child:Column(
                              children: [
                                Container(
                                  height: 210,
                                  width: 210,
                                  clipBehavior: Clip.hardEdge,
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: Image.asset(DataLegumes.imageName,fit: BoxFit.cover,),
                                ),
                                SizedBox(height: 20,),
                                Text(DataLegumes.title,
                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 20,),
                                Text("Temperature: " + DataLegumes.temperature.toString()+ " °C",
                                style:TextStyle(fontSize: 14,color: Colors.grey.shade600)
                                ),
                                SizedBox(height: 10,),
                                Text("Humidité: " + DataLegumes.humidite.toString()+ " %",
                                style:TextStyle(fontSize: 14,color: Colors.grey.shade600)
                                ),
                                SizedBox(height: 10,),
                                Text("Temps de séchage: " + DataLegumes.humidite.toString()+ " h",
                                style:TextStyle(fontSize: 14,color: Colors.grey.shade600)
                                ),
                              ],
                            ),
                             ),
                        ),
                      ),
                    ),
                    SizedBox(height: 50,),
                    if (_selectedLegume==DataLegumes)
                    ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                         MaterialPageRoute(builder: ((context) => DebutSech())));
                      }, 
                      child: Text("Demarrer "))
                  ],
                );
              }
              );
          }).toList()
          ),
      ),
    );
  }
}


class DataLegumes{
  final String title;
  final String imageName;
  final int temperature;
  final int humidite;
  final int temp_sech;
  DataLegumes({
    required this.title,
    required this.imageName,
    required this.temperature,
    required this.humidite,
    required this.temp_sech
  });

  static map(Builder Function(dynamic Legumes) param0) {}
}
 final List <DataLegumes> datalist=[
  DataLegumes(title: "Tomate", imageName: "images/tomate.png", temperature: 60, humidite: 32, temp_sech: 6),
  DataLegumes(title: "Poivre", imageName: "images/poivre.png", temperature: 55, humidite: 30, temp_sech: 5),
  DataLegumes(title: "Choufleur", imageName: "images/choufleur.png", temperature: 70, humidite: 28, temp_sech: 8),
  DataLegumes(title: "Chou", imageName: "images/Chou.png", temperature: 65, humidite: 25, temp_sech: 7),
  DataLegumes(title: "Carotte", imageName: "images/carotte.png", temperature: 80, humidite: 35, temp_sech: 9),
  DataLegumes(title: "Betterave", imageName: "images/betterave.png", temperature: 75, humidite: 39, temp_sech: 6),
];