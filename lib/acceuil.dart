

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sech_app/fruits.dart';
import 'package:sech_app/legumes.dart';
import 'package:sech_app/main.dart';
import 'package:sech_app/settings.dart';




void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}
class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

 bool isClic=false;
 bool isClic2=false;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Selectionner  '),
        centerTitle: true,
        leading: IconButton(
          icon:Icon(Icons.logout,color: Colors.black),
          onPressed: (){
            Navigator.pop(context,
            MaterialPageRoute(builder: (context)=> MyHomePage()));
          },
           ),
      actions: [
          Image.asset('images/logo_gep.png'),
      ],
      elevation: 0,
      backgroundColor:Colors.transparent ,
      ),
      
      body:Center(
           child: Column(
             children: [
               Padding(padding: EdgeInsets.only(top:70)),
               Material(
                 elevation: 8,
                 borderRadius: BorderRadius.circular(20),
                 color: Colors.teal,
                 child: InkWell(
                   onTap: (){
                     Navigator.push(context,
                     MaterialPageRoute(builder: (context)=>Fruits()));
                   },
                   child: Ink.image(
                     image: AssetImage('images/fruit.png'),
                     height:150,
                     width:150,
                     fit: BoxFit.cover,
                     )
                 ),
               ),
               Padding(padding: EdgeInsets.only(top:80)),
               Material(
                 elevation: 8,
                 borderRadius: BorderRadius.circular(20),
                 color: Colors.teal,
                 child: InkWell(
                   splashColor: Colors.transparent,
                   onTap: (){
                     Navigator.push(context,
                     MaterialPageRoute(builder: (context)=>Legumes()));
                   },
                   child: Ink.image(
                     image: AssetImage('images/legume.png'),
                     height: 150,
                     width: 150,
                     fit: BoxFit.cover,
                     ),
                 ),
               )
             ],
           ),
          ),
      );
  }
}
