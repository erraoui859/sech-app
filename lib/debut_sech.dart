import 'package:flutter/material.dart';
import 'package:sech_app/acceuil.dart';

void main ()=>runApp(MyApp());

class DebutSech extends StatefulWidget {

  @override
  State<DebutSech> createState() => _DebutSechState();
}

class _DebutSechState extends State<DebutSech> {
  Duration duration=Duration();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color:Colors.black),
          onPressed: (){
            Navigator.pop(context,
            MaterialPageRoute(builder: (context)=>Home()));
          },
        ),
      ),
      body:  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Séchage en cours ',style: TextStyle(fontSize:80,fontWeight: FontWeight.bold)),
                SizedBox(height: 30,),
                buildTime(),
                SizedBox(height:70),
                ElevatedButton(
                  onPressed: (){},
                   child: Text("Arreter",style: TextStyle(fontSize: 30,color: Colors.black),)
                   ),
              ],
            ),
      )
    );
  }
  Widget buildTime() {
    String twoDigits (int n)=>n.toString().padLeft(2,"0");
    final heures=twoDigits(duration.inHours.remainder(60));
    final minutes= twoDigits(duration.inMinutes.remainder(60));
    final secondes=twoDigits(duration.inSeconds.remainder(60));
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
       buildTimeCard(time:heures,header:"HEURES"),
       SizedBox (width: 10,),
       buildTimeCard(time:minutes,header:"MINUTES"),
       SizedBox (width: 10,),
       buildTimeCard(time:secondes,header:"SECONDES")
      ],
    );
  }
  Widget buildTimeCard ({required String time,required String header})=>
  Column(
    children: [
      Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color:Colors.black54
        ),
        child: Text(time,
        style: TextStyle(fontSize: 50,fontWeight: FontWeight.bold,color: Colors.white),
        ),
      ),
      SizedBox(height: 10,),
      Text(header),
    ],
  );
}