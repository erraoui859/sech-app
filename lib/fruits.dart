import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/material.dart';
import 'package:sech_app/acceuil.dart';
import 'package:sech_app/debut_sech.dart';

void main ()=>runApp(MyApp());

class Fruits extends StatefulWidget {

  @override
  State<Fruits> createState() => _FruitsState();
}

class _FruitsState extends State<Fruits> {


   int _current=0;
   dynamic _selectedFruit={};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
       title: Text('Selectionner votre fruit',
       style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 25)
       ),
       leading: IconButton(
         icon:Icon(Icons.arrow_back,color: Colors.black,),
         onPressed:(){
           Navigator.pop(context,
           MaterialPageRoute(builder: (context)=> Home()));
         }
          ),
       centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: CarouselSlider(
          options: CarouselOptions(
            height: 450,
            aspectRatio: 16/9,
            viewportFraction: 0.70,
            enlargeCenterPage: true,
          ),
          items: dataliste.map((DataFruit) {
            return Builder(
              builder: (context){
                return Column(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            if (_selectedFruit==DataFruit)
                            _selectedFruit={};
                            else
                            _selectedFruit=DataFruit;
                          });
                        },
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 300),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            border: _selectedFruit==DataFruit ? Border.all(
                              color: Colors.green.shade500,
                              width: 3,
                            ):null,
                            boxShadow: _selectedFruit==DataFruit ?    [
                              BoxShadow(
                                color: Colors.green.shade100,
                                blurRadius: 30,
                                offset: Offset(0,10),
                              ),
                              ]: [
                              BoxShadow(
                                color:Colors.grey.withOpacity(0.2),
                                blurRadius: 20,
                                offset: Offset(0,5),
                              )
                            ],
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Container(

                                  height:210,
                                  width: 500,
                                  clipBehavior: Clip.hardEdge,
                                  margin: EdgeInsets.only(top:10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: Image.asset(DataFruit.image,fit: BoxFit.fill,),
                                ),
                                SizedBox(height:20),
                                Text(DataFruit.title,
                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height:20),
                                Text("Temperature: "+ DataFruit.tempe.toString() +" °C",
                                style: TextStyle(fontSize: 14,color:Colors.grey.shade600)
                                ),
                                SizedBox(height: 10,),
                                Text("Humidité: "+ DataFruit.hum.toString()+" %",
                                style: TextStyle(fontSize: 14,color:Colors.grey.shade600),
                                ),
                                SizedBox(height: 10,),
                                Text("Temps de séchage: " +DataFruit.temp_seche.toString()+" h",
                                style: TextStyle(fontSize: 14,color:Colors.grey.shade600),
                                ),
                                SizedBox(height: 10,),
                              ]
                              ),
                          ),
                          ),
                    
                      ),
                    ),
                    SizedBox(height: 50,),
                    if (_selectedFruit==DataFruit)
                    ElevatedButton(
                      onPressed: (){
                        Navigator.push(context,
                         MaterialPageRoute(builder: ((context) => DebutSech())));
                      }, 
                      child: Text("Demarrer "))
                  ],
                );
              }
              );
          }).toList(),
        )
      )
      );
  }
  }



  class DataFruit {
  final String title;
  final String image;
  final int tempe;
  final int hum;
  final int temp_seche;
  DataFruit({
    required this.title, 
    required this.image,
    required this.tempe,
    required this.hum,
    required this.temp_seche,
    });
}

 final List <DataFruit> dataliste=[
  DataFruit(title: "Fraise", image: "images/fraises.png", tempe: 60, hum: 32, temp_seche: 6),
  DataFruit(title: "Banane", image: "images/bananes.png", tempe: 55, hum: 35, temp_seche: 7),
  DataFruit(title: "Figue", image: "images/figue.jpg", tempe: 70, hum: 29, temp_seche: 8),
  DataFruit(title: "Orange", image: "images/orange.jpg", tempe: 85, hum: 30, temp_seche: 5),
  DataFruit(title: "Peche", image: "images/peches.jpg", tempe: 65, hum: 25, temp_seche: 9),
  DataFruit(title: "Pomme", image: "images/pomme.jpg", tempe: 40, hum: 39, temp_seche: 8),
  DataFruit(title: "Raisin", image: "images/raisins.jpg", tempe: 80, hum: 32, temp_seche: 5),
  DataFruit(title: "Abricot", image: "images/abricot.jpg", tempe: 75, hum: 31, temp_seche: 7),
];

  
  
