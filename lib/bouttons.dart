
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:sech_app/acceuil.dart';
import 'package:sech_app/debut.dart';
import 'package:sech_app/main.dart';
import 'package:sech_app/settings.dart';


void main()=>runApp(Bouttons());

class Bouttons extends StatefulWidget {
  
  @override
  State<Bouttons> createState() => _BouttonsState();
}

class _BouttonsState extends State<Bouttons> {
  GlobalKey<CurvedNavigationBarState> _NavKey=GlobalKey();
  var AllPages=[
    Home(),
    Settings(),
  ];
  var myindex=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CurvedNavigationBar( 
        backgroundColor: Colors.transparent,
        key:_NavKey,
        items: [
          Icon(Icons.home),
          Icon(Icons.settings),
        ],
        buttonBackgroundColor:Colors.transparent,
        onTap: (index){
          setState(() {
            myindex=index;
          });
        },
        animationCurve:Curves.fastLinearToSlowEaseIn,color:Colors.green,
        ),
        body: AllPages[myindex],
    );
  }
}