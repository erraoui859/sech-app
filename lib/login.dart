import 'dart:ui';


import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:sech_app/acceuil.dart';
import 'package:sech_app/main.dart';
import 'package:lottie/lottie.dart';
import 'package:sech_app/bouttons.dart';
import 'package:firebase_auth/firebase_auth.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
  }

class MyApp extends StatelessWidget {
 @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {


  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool mdp=true;
  void initState(){
    super.initState();
  }
  final _formkey = GlobalKey<FormState>();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _passwordcontroller = TextEditingController();
  void dispose(){
    _emailcontroller.dispose();
    _passwordcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size=MediaQuery.of(context).size;
    return Scaffold(
      appBar:AppBar(
        actions: [
          Image.asset('images/logo_gep.png'),
        ],
        elevation: 0,
        backgroundColor: Colors.white24,
        ),
      body:SingleChildScrollView(
        child: Padding(padding:EdgeInsets.symmetric(horizontal:size.width/30),
          child: Form(
            key: _formkey,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget> [
                  LottieBuilder.asset('images/farme.json',height: 350),
                SizedBox(height:20),
                  TextFormField(
                    decoration:InputDecoration(
                     label: Text('Email',style:TextStyle(fontSize:20,color: Colors.teal)),
                     contentPadding: EdgeInsets.only(left: 15),
                     hintText:'Saisir votre email',
                     
                     icon:Icon(Icons.mail,color:Colors.teal,size:30),
                     border: UnderlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(45))),
                    ),
                    validator: (val){
                      if(val!.isEmpty){
                        return 'Entrer votre email';
                      }
                    },
                    keyboardType: TextInputType.text ,
                  ),
                   SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(
                      label: Text('Password',style:TextStyle(fontSize:20,color: Colors.teal)),
                      contentPadding: EdgeInsets.only(left: 15),
                      hintText:'Entrer le mot de passe',
                      icon:Icon(
                        Icons.lock,
                        color:Colors.teal,
                        size:30,
                      ),
                      border: UnderlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(45))),
                      suffixIcon:IconButton(
                        icon:Icon(Icons.remove_red_eye),
                        onPressed:(){
                          setState(() {
                            mdp=!mdp;
                          });
                        }
                        ),
                       
                    ),
                    validator: (val){
                      if(val!.isEmpty){
                        return 'Entrer votre mot de passe';
                      }
                    },
                    keyboardType: TextInputType.text,
                    obscureText: mdp,
                  ),
                  SizedBox(height: 50),
                  ElevatedButton(
                    onPressed: () async {
                      if(_formkey.currentState!.validate()){
                        var result = await FirebaseAuth.instance.signInWithEmailAndPassword(
                          email: _emailcontroller.text, password: _passwordcontroller.text);
                          if(result!=null){
                            Navigator.pushReplacement(context, 
                            MaterialPageRoute(builder: ((context) => Bouttons())));
                          } 
                      }
                    },
                    style:ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.teal),
                      padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical:20,horizontal:100)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      side:BorderSide(color: Colors.grey),
                      borderRadius:BorderRadius.circular(35) ,
                      )),
                      elevation: MaterialStateProperty.all(0),
                    ) ,
                     child: Text('Login',style: TextStyle(color:Colors.black,fontSize:27) ,)
                    ),
                ],
              ),
          ),
        )
        ),
    );
  }
}
